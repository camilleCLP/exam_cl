# EXAMEN CONCEPTION LOGICIELLE

## Description
Ce projet est constitué d'une partie serveur permettant d'exposer une API et d'une partie client comportant un scénario d'utilisation de cette API.<br/>
L'objectif de l'API est de renvoyer une chanson aléatoire d'un artiste donné par l'utilisateur.<br/>
Le scénario client est le suivant : un utilisateur dispose d'une liste d'artistes au format json. Il peut aussi mettre des notes aux artistes afin de tirer des chanson de ses artistes préférés avec des probabilités plus élevées. Il pourra ensuite générer une playlist de chansons aléatoires basées sur ses artistes préférés.
## Pré-requis
Python 3.8 ou +

## Installation
```shell
git clone https://gitlab.com/camilleCLP/exam_cl.git
cd exam_cl
pip install -r requirements.txt
```
## Schéma d'architecture macro
```mermaid
graph RL
    G[Client] --> |requête HTTP| E
    E[Serveur] --> |requête HTTP| D
    E[Serveur] --> |requête HTTP| F
    D[API AudioDB] --> |JSON| E
    F[API Lyrics Ovh] --> |JSON| E
    E --> |JSON| G
```

## Organisation du code
### Package serveur
- *artist.py* : contient des méthodes permettant de récupérer des informations sur un artiste
- *album.py* : contient des méthodes pour récupérer des informations sur un album
- *song.py* : utilise les méthodes de *artist* et *album* pour afficher une chanson aléatoirement
- *verif_services.py* : contient des méthodes permettant de tester le bon fonctionnement de AudioDB et LyricsOvh
- *test-song.py* : tests unitaires vérifiant le bon fonctionnement de *song*
- *main.py* : utilise *song* et *verif_services* pour mettre en place le webservice avec les endpoints demandés
- *test_main.py* : tests unitaires vérifiant le bon fonctionnement du webservice développé

### Package client
- *main.py* : requête le webservice développé et génère la playlist de l'utilisateur

## Environnement
Les variables d'environnement peuvent être modifiées dans le fichier .env. <br/>
La variable booléenne LYRICS_PRESENCE dans ce fichier permet de spécifier si l'on souhaite que les chansons renvoyées aient nécessairement des paroles ou non. Elle vaut True par défaut car on peut supposer que l'utilisateur souhaitera disposer des paroles des chansons. Cependant, il est tout à fait possible de modifier la valeur de cette variable à False si l'on souhaite relâcher cette contrainte.
## Lancement du webservice et du scénario client
### Serveur
```shell
python serveur/main.py
```
Une fois le serveur lancé, il est possible d'accéder à l'API et d'utiliser deux endpoints :
- `/` teste la bonne configuration et la disponibilité des services dont dépend l'API
- `/random/{artist_name}` renvoie des informations sur une chanson tirée aléatoirement de l'ariste nommé {artist_name}.
### Client
Le client génère une playlist aléatoire à partir des chansons et des notes données dans le fichier json. <br/>
Une chanson ne peut pas apparaître plusieurs fois par défaut. On peut modifier ce paramètre dans le code en changeant la valeur de unique_song à False. De plus, si des notes ont été fournies pour tous les artistes de la playlist alors le tirage aléatoire favorisera les artistes les mieux notés, sinon le tirage est uniforme.
Pour exécuter le client, ouvrir un nouveau terminal puis exécuter :
```shell
cd exam_cl
python client/main.py
```
## Lancement des tests unitaires
Pour commencer, on peut exécuter le module `test_song.py` qui teste les méthodes utilisées par l'API. Pour cela, ouvrir un terminal puis exécuter :
```shell
cd exam_cl/serveur
python -m unittest test_song.py
python main.py
```
Puis, on peut vérifier le bon fonctionnement de notre API en ouvrant un nouveau terminal et en exécutant :
```shell
cd exam_cl/serveur
python -m unittest test_main.py
```