import json
import os
import dotenv
import requests
import numpy

# nombre de chansons dans la playlist
length_playlist = 20
# indique si la chanson doit aparaître une unique fois dans la playlist
unique_song = False 

# ouverture du fichier json
dotenv.load_dotenv()
my_file=open(str(os.getenv("MY_PLAYLIST")))
my_artists = json.load(my_file)
# tirage des artistes en fonction des notes données
my_fav_artist = []
my_fav_notes = []
for artist in my_artists:
    my_fav_artist.append(artist["artiste"])
    if artist["note"]:
        my_fav_notes.append(artist["note"])
    else:
        my_fav_notes = []

# Si on a des notes, on tire avec en favorisant les notes élevées sinon tirage uniforme
if len(my_fav_notes) == length_playlist:
    tot = sum(my_fav_notes)
    prob = []
    for elem in my_fav_notes:
        prob.append(elem/tot)
    artists = numpy.random.choice(my_fav_artist, size = length_playlist, p=prob)
else:
    artists = numpy.random.choice(my_fav_artist, size = length_playlist)

# playlist
my_playlist = []
for artist in artists:
    req = requests.get(str(os.getenv("SERVER_URL")) + "/random/" + artist)
    if req.status_code == 200:
        res = req.json()
        # si la chanson n'est pas dans la playlist on ajoute
        if not unique_song or res not in my_playlist:
            my_playlist.append(res)
        # sinon on requête le serveur jusqu'à obtenir une chanson pas encore ajoutée
        # au maximum on rappelle le serveur 10 fois
        else:
            cpt = 0
            while cpt < 10 and res not in my_playlist:
                req = requests.get(str(os.getenv("SERVER_URL")) + "/random/" + artist)
                res = req.json()
            if res not in my_playlist:
                my_playlist.append(res)
            else:      
                my_playlist = []
                raise Exception("The artist " + artist + \
                    " does not have enough songs to make a playlist of length " + str(length_playlist)\
                    + "\n Add artists to your file or reduce the length of the playlist")
    else:
        raise Exception("We do not find any song for the artist " + artist)
if my_playlist:
    print(my_playlist)

if __name__ == "__main__":
    dotenv.load_dotenv()