from typing import Optional
from fastapi import FastAPI, Response, status
import os
import dotenv
import uvicorn
from verif_services import VerifServices
from song import Song

app = FastAPI()

@app.get("/")
def read_root(response: Response):
    '''
    Vérifier les réponses de l'API quand les données entrées existent ou non
    '''
    # Données à tester
    test = VerifServices()
    # Codes et libellés existants normalement
    name_artist = "Coldplay"
    id_artist = "111611"
    id_album = "2199391"
    name_song = "Paradise"
    # Codes et libellés inexistants normalement
    name_artist2 = "Coldplayyy"
    id_artist2 = "000611"
    id_album2 = "0009391"
    name_song2 = "Paradiseee"
    if test.test_api1_ok('https://www.theaudiodb.com/api/v1/json/2/search.php?s='+ name_artist, name_artist) \
        and test.test_api1_ok('https://theaudiodb.com/api/v1/json/2/album.php?i=' + id_artist, id_artist) \
        and test.test_api1_ok('https://theaudiodb.com/api/v1/json/2/track.php?m=' + id_album, id_album) \
        and test.test_api2_ok('https://api.lyrics.ovh/v1/+' + name_artist + '/' + name_song, "When she was just a girl") \
        and test.test_api1_not_ok('https://www.theaudiodb.com/api/v1/json/2/search.php?s=' + name_artist2,  "artists") \
        and test.test_api1_not_ok('https://theaudiodb.com/api/v1/json/2/album.php?i=' + id_artist2,  "album") \
        and test.test_api1_not_ok('https://theaudiodb.com/api/v1/json/2/track.php?m=' + id_album2,  "track") \
        and test.test_api2_not_ok('https://api.lyrics.ovh/v1/+' + name_artist2 + '/' + name_song2):
        response.status_code = status.HTTP_200_OK
        return "Welcome in this API! \
            You can find information about a randomly chosen song of your favorite artist. \
            Use this endpoint: /random/{artist_name} to do so"
    else:
        response.status_code = status.HTTP_503_SERVICE_UNAVAILABLE
        return "Services used by our API are temporarily unavailable!"

@app.get("/random/{artist_name}")
def read_artist(artist_name:str, response: Response, q: Optional[str] = None):
    '''
    Renvoyer les informations sur une chanson tirée aléatoirement 
    à partir de celles d'un artiste choisi 
    '''
    song = Song.parse_obj({"artist_name": artist_name})
    res = song.find_song(os.getenv("LYRICS_PRESENCE"))
    if res:
        response.status_code = status.HTTP_200_OK
    else:
        res['error'] = 'artist not found'
        response.status_code = status.HTTP_404_NOT_FOUND
    return res

if __name__ == "__main__":
    dotenv.load_dotenv()
    uvicorn.run(app, host=str(os.getenv("HOST")), port=int(os.getenv("TARGET_PORT")))