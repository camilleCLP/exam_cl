from typing import List
from pydantic import BaseModel
import requests

class Artist(BaseModel):
    name: str

    def get_id(self) -> None:
        '''
        Récupère l'identifiant d'un artiste identifié par son nom (name)
        '''
        req = requests.get('https://www.theaudiodb.com/api/v1/json/2/search.php?s='+ self.name)
        id_artist = None 
        if req.status_code == 200:
            res  = req.json()
            artist = res['artists']
            if artist:
                id_artist = artist[0]['idArtist']
        return id_artist

    def get_albums(self, id: int) -> List[str]:
        '''
        Retourne les identifiants des albums de l'artiste
        Si aucun album, retourne une liste vide
        '''
        album = []
        if id:
            req = requests.get('https://theaudiodb.com/api/v1/json/2/album.php?i='+ str(id))
            if req.status_code == 200:
                res = req.json()
                for i in range(len(res['album'])):
                    album.append(res['album'][i]['idAlbum'])
        return album

    def get_lyric_song(self, song: str):
        '''
        Retourne les paroles d'une chanson de l'artiste
        '''
        req = requests.get('https://api.lyrics.ovh/v1/' + self.name.lower() + '/'+ song)
        lyrics = None
        if req.status_code == 200:
            res = req.json()
            if "error" not in res.keys():
                lyrics = res['lyrics']
        return lyrics