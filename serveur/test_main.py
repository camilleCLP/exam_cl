import requests
import os
import dotenv
import unittest

class TestMain(unittest.TestCase):
    def test_read_artist(self):
        # GIVEN
        artist1 = "coldplay"
        artist2 = "coldplayyyyyyy"
        # WHEN
        dotenv.load_dotenv()
        r1 = requests.get(str(os.getenv("SERVER_URL")) + "/random/" + artist1)
        r2 = requests.get(str(os.getenv("SERVER_URL")) + "/random/" + artist2)
        alea_song1 = r1.json()
        alea_song2 = r2.json()
        # THEN
        assert r1.status_code == 200
        assert alea_song1['artist'].lower() == artist1.lower()
        assert r2.status_code == 404
        assert alea_song2['error'] == 'artist not found'

if __name__ == '__main__':
    unittest.main()