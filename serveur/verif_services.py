import requests

class VerifServices():
    """
    Cette classe permet de vérifier le bon fonctionnement et le temps de réponse
    des 2 API requêtées
    """
    def test_api1_ok(self, link_api: str, expected_result: str):
        '''
        Vérifie pour un exemple type de requête sur AudioDB qu'on obtient 
        bien le résultat attentu en cas de requête correcte, avec un code 
        HTTP adéquat et en temps raisonnable
        '''
        test = False
        req = requests.get(link_api)
        if req.status_code == 200 and req.elapsed.total_seconds()<3:
            res = req.json()
            for key in res.keys():
                for elem in res[key]:
                    if expected_result in elem.values():
                        test = True
        return test

    def test_api2_ok(self, link_api: str, expected_result: str):
        '''
        Vérifie pour un exemple type de requête sur LyricsOvh qu'on obtient 
        bien le résultat attentu en cas de requête correcte, avec un code 
        HTTP adéquat et en temps raisonnable
        '''
        test = False
        req = requests.get(link_api)
        if req.status_code == 200 and req.elapsed.total_seconds()<3:
            res = req.json()
            if expected_result in res["lyrics"]:
                test = True
        return test

    def test_api1_not_ok(self, link_api: str, searched_elem: str):
        '''
        Vérifie pour un exemple type de requête sur AudioDB qu'on obtient 
        bien le résultat attentu en cas de requête incorrecte, avec un code 
        HTTP adéquat et en temps raisonnable
        '''
        test = False
        req = requests.get(link_api)
        if req.status_code == 200 and req.elapsed.total_seconds()<3:
            res = req.json()
            if not res[searched_elem]:
                test = True
        return test

    def test_api2_not_ok(self, link_api: str):
        '''
        Vérifie pour un exemple type de requête sur LyricsOvh qu'on obtient 
        bien le résultat attentu en cas de requête incorrecte, avec un code 
        HTTP adéquat et en temps raisonnable
        '''
        test = False
        req = requests.get(link_api)
        if req.status_code == 404 and req.elapsed.total_seconds()<3:
            res = req.json()
            if res["error"]:
                test = True
        return test

    
