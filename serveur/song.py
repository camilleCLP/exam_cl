import random
from pydantic import BaseModel
from artist import Artist
from album import Album

class Song(BaseModel):
    artist_name: str

    def find_song(self, lyrics_presence: bool = False, cpt: int = 0) -> dict:
        '''
        Trouve aléatoirement une chanson à partir du nom de l'artiste
        lyrics_presence permet de spécifier si les paroles de la chanson 
        renvoyée doivent nécessairement être présentes ou non.
        Lorsque lyrics_presence vaut True, la fonction va vérifier l'existence
        de paroles.
        En cas d'absence, la fonction s'appelle récursivement 10 fois au max
        afin de choisir une chanson dont les paroles sont connues.
        Si on ne parvient pas à trouver les paroles au bout de 10 récursions,
        le résultat renvoyé sera vide.
        '''
        res = {}
        # On appelle au maximum 10 fois par récursion
        if cpt < 10:
            # Récupérer un album de l'artiste aléatoirement
            a = Artist.parse_obj({'name': self.artist_name})
            id_artist = a.get_id()
            albums = a.get_albums(id_artist)
            if albums:
                chosen_album = albums[random.randint(0,len(albums)-1)]
                # Récupérer une chanson de l'album aléatoirement
                album = Album.parse_obj({'id': chosen_album})
                songs = album.get_songs()
                chosen_song = songs[random.randint(0,len(songs)-1)]
                # Récupérer les informations de la chanson
                artist_lower = self.artist_name.lower()
                title = chosen_song['title']
                suggested_url = chosen_song['suggested_youtube_url']
                lyrics = a.get_lyric_song(title)
                # S'il n'y a pas de paroles et qu'on souhaite en avoir 
                # alors on appelle récursivement
                if lyrics_presence and not lyrics:
                    return self.find_song(True, cpt + 1)
                # Mettre en forme les résultats
                res['artist'] = artist_lower
                res['title'] = title
                res['suggested_youtube_url'] = suggested_url
                res['lyrics'] = lyrics
        return res