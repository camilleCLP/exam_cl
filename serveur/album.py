from typing import List
from pydantic import BaseModel
import requests

class Album(BaseModel):
    id: int
    
    def get_songs(self) -> List[str]:
        '''
        Retourne les chansons de l'album identifié par son identifiant (id)
        '''
        songs = []
        req = requests.get('https://theaudiodb.com/api/v1/json/2/track.php?m=' + str(self.id))
        if req.status_code == 200:
            res = req.json()
            if res['track']:
                for i in range(len(res['track'])):
                    track = res['track'][i]
                    song = {'title': track['strTrack'], 'suggested_youtube_url': track['strMusicVid']}
                    songs.append(song)
        return songs