import unittest
from song import Song

class TestSong(unittest.TestCase):
    def test_find_song(self):
        # GIVEN
        artists = ['daft punk', 'gloria gaynor', 'boney m', 'oasis', 'Bob Marley']         
        for artist in artists:
            # WHEN
            song = Song.parse_obj({'artist_name': artist})
            found_song = song.find_song(lyrics_presence=True)
            # THEN
            # vérifier que la chanson trouvée est bien celle de l'artiste voulu
            self.assertEqual(found_song['artist'].lower(), artist.lower())
            # vérifier que l'on a bien des paroles
            self.assertIsNotNone(found_song['lyrics'])

if __name__ == '__main__':
    unittest.main()